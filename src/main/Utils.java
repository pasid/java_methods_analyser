package main;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author Airton
 *
 */
class Utils {

	/**
	 * 
	 * @param lines
	 * @return
	 * 
	 * The rules of PMD tool () were used to build this method, which says : 
	 * 
	 * Methods have a base complexity of 1.
	 * 
	 * +1 for every control flow statement (if, case, catch, throw, do, while, for, break, continue) and conditional expression (?:) [Sonarqube]. 
	 * Notice switch cases count as one, but not the switch itself: the point is that a switch should have the same complexity value as the equivalent series of if statements.
	 * else, finally and default don�t count;
	 * 
	 * +1 for every boolean operator (&&, ||) in the guard condition of a control flow statement. 
	 * That�s because Java has short-circuit evaluation semantics for boolean operators, which makes every boolean operator kind of a control flow statement in itself.
	 * 
	 * https://pmd.sourceforge.io/snapshot/pmd_java_metrics_index.html#access-to-foreign-data-atfd
	 */
	public static int getCyclomaticComplexity(ArrayList<String> lines) {
		int complexity = 1;


		String[] keywords = {"if","while","case","for","switch","do","continue","break","&&","||","?","catch","throw","throws","return"};
		String words = "";

		for (String line : lines) {
			StringTokenizer stTokenizer = new StringTokenizer(line);
			while (stTokenizer.hasMoreTokens())
			{
				words = stTokenizer.nextToken();
				for(int i=0; i<keywords.length; i++)
				{
					if (keywords[i].equals(words))
					{
						complexity++;
					}
				}
			}
		}				

		return (complexity);
	}

	public static ArrayList<String> getCodeLines(String nome) {
		ArrayList<String> list = new ArrayList<>();
		Scanner ler = new Scanner(System.in);
		try {
			FileReader arq = new FileReader(nome);
			BufferedReader lerArq = new BufferedReader(arq);

			String linha = lerArq.readLine(); 
			if (linha != null) {
				list.add(linha.trim());
			}
			while (linha != null) {
				list.add(linha.trim());
				linha = lerArq.readLine(); 
			}

			arq.close();
		} catch (IOException e) {
			System.err.printf("Erro na abertura do arquivo: %s.\n",
					e.getMessage());
		}

		return list;
	}

	public static ArrayList<MethodStats> analyseMethods(ArrayList<String> list) {
		ArrayList<MethodStats> listOfMethods = new ArrayList<>();

		MethodStats lastMethod = null;
		for (String line : list) {
			if (isMethodDeclaration(line)) {
				String methodSignature = line.replace("{", "");

				if (!methodSignature.contains(")")) {
					methodSignature += list.get(list.indexOf(line)+1);
				
				}

				lastMethod = new MethodStats(methodSignature) ;
				
				
				
				ArrayList<String> parameters = getMethodParameters(methodSignature);


				lastMethod.setParameters(parameters);

				listOfMethods.add(lastMethod);
			}

			else if (!listOfMethods.isEmpty() && !line.isEmpty()) {
				lastMethod.getLines().add(line);
			}
		}

		for (MethodStats methodStats : listOfMethods) {
			methodStats.calculateCyclomaticComplexity();
		}

		return listOfMethods;
	}

	public static ArrayList<String> getMethodParameters(String methodSignature) {
		Pattern p = Pattern.compile("[\\w\\s$]*(\\((.*[\\w\\s,$]*)\\))");
		Matcher m = p.matcher(methodSignature);
		m.find();
		String parameters = null ;
		try {
			parameters = m.group(2);
		} catch (IllegalStateException e) {
			e.printStackTrace();
		}

		ArrayList<String> parameterList = new ArrayList<String>(Arrays.asList(parameters.split(",")));

		ArrayList<String> emptyItens = new ArrayList<String>();
		for (String item : parameterList) {
			if (item.isEmpty()) {
				emptyItens.add(item);
			}
		}
		parameterList.removeAll(emptyItens);
		return parameterList;
	}	

	public static boolean isMethodDeclaration(String s) {
//		String regexMethodDeclaration = "((public|private|protected|static|final|native|synchronized|abstract|transient)+\\s)+[\\$_\\w\\<\\>\\[\\]]*\\s+[\\$_\\w]+\\([^\\)]*\\)?\\s*\\{?[^\\}]*\\}?";
		String regexMethodDeclaration = "((public|private|protected|static|final|native|synchronized|abstract|transient)+\\s)+[\\w\\<\\>\\[\\]]+\\s+(\\w+) *\\([^\\)]*\\) *(\\{?|[^;])";
		

		return s.matches(regexMethodDeclaration);
	}

	public static Collection<File> listAllJavaFilesRecursivelly(File dir) {
		    Set<File> fileTree = new HashSet<File>();
		    if(dir==null||dir.listFiles()==null){
		        return fileTree;
		    }
		    for (File entry : dir.listFiles()) {
		    		if (entry.isFile() && entry.getAbsolutePath().endsWith("java")) {
		    			fileTree.add(entry);
		    		}
		    		else fileTree.addAll(listAllJavaFilesRecursivelly(entry));
		    }
		    return fileTree;
		}
	    

}