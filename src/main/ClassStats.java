package main;

import java.util.ArrayList;

public class ClassStats {

	private String className;
	private ArrayList<MethodStats> methods;
	private ArrayList<String> lines;
	private int linesOfCode;
	private int cyclomaticComplexity;

	
	public ClassStats(String className) {
		methods = new ArrayList<>();
		this.setClassName(className);
		this.setLines(Utils.getCodeLines(className));
	}

	public void analyzeClass() {
		setMethods(Utils.analyseMethods(lines));
		int loc= 0;
		int cyclo = 0 ;
		for (MethodStats m : this.getMethods()) {
			loc+=m.getNumberOfLines();
			cyclo +=m.getCyclocomplexity();
		}
		
		this.setLinesOfCode(loc);
		this.setCyclomaticComplexity(cyclo);
	}

	public ArrayList<String> getLines() {
		return lines;
	}

	public void setLines(ArrayList<String> lines) {
		this.lines = lines;
	}

	public ArrayList<MethodStats> getMethods() {
		return methods;
	}

	public void setMethods(ArrayList<MethodStats> methods) {
		this.methods = methods;
	}
	
	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}
	

	public int getLinesOfCode() {
		return linesOfCode;
	}

	public void setLinesOfCode(int linesOfCode) {
		this.linesOfCode = linesOfCode;
	}

	public int getCyclomaticComplexity() {
		return cyclomaticComplexity;
	}

	public void setCyclomaticComplexity(int cyclomaticComplexity) {
		this.cyclomaticComplexity = cyclomaticComplexity;
	}

	@Override
	public String toString() {
		
		String output =null;
		output = "\nClass Name ; Lines Of Code ; Number Of Methods ; Cyclomatic Complexity \n";
		output+= this.getClassName() + ";" + this.getLinesOfCode() + ";" + this.getMethods().size() + ";" + getCyclomaticComplexity() + "\n\n";
		
		
		output+="Method Signature ; Cyclomatic Complexity ; Number of Lines ; Number of Parameters \n"; // HEADER OF METHODS LIST
		for (MethodStats methodStats : methods) {
			output += methodStats+"\n";
		}
		
		return output;
	}
	
}
