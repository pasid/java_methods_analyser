package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * 
 * @author Airton
 *
 */
public class ProjectStats {

	private ArrayList<ClassStats> classes;
	private int linesOfCode;
	private int cyclomaticComplexity;

	public ProjectStats() {
		this.classes = new ArrayList<>();
	}

	public ArrayList<ClassStats> getClasses() {
		return classes;
	}

	public void setClasses(ArrayList<ClassStats> classes) {
		this.classes = classes;
	}

	public int getLinesOfCode() {
		return linesOfCode;
	}

	public void setLinesOfCode(int linesOfCode) {
		this.linesOfCode = linesOfCode;
	}

	public int getCyclomaticComplexity() {
		return cyclomaticComplexity;
	}

	public void setCyclomaticComplexity(int cyclomaticComplexity) {
		this.cyclomaticComplexity = cyclomaticComplexity;
	}

	public void analyzeProject() {
		int loc= 0;
		int cyclo = 0 ;
		for (ClassStats classStats : classes) {
			classStats.analyzeClass();
			loc+=classStats.getLinesOfCode();
			cyclo +=classStats.getCyclomaticComplexity();
		}
		this.setLinesOfCode(loc);
		this.setCyclomaticComplexity(cyclo);
	}


	@Override
	public String toString() {
		String output = null;
		
		output = "# Summary of Project #\n";
		output+= "Lines of Code; Cyclomatic Complexity; Number of Classes\n";
		output+= this.getLinesOfCode() +";"+ cyclomaticComplexity + ";" + this.getClasses().size() + "\n\n";
		
		
		for (ClassStats classStats : classes) {
			output += classStats.toString();
		}
		
		return output;
	}
	
	
	public void printReport(boolean oncsv) throws FileNotFoundException {
		if (oncsv) {
			PrintWriter pw = new PrintWriter(new File("report.csv"));
			StringBuilder sb = new StringBuilder();
			sb.append(this.toString());
			pw.write(sb.toString());
			pw.close();
		}else {
			System.out.println(this.toString());
		}
		System.out.println("The report.csv was generated!");
	}



























}
