package main;

import java.util.ArrayList;

/**
 * 
 * @author Airton
 *
 */
public class MethodStats {
	private String methodSignature;
	private int cycloComplexity;
	private ArrayList<String> lines;
	private ArrayList<String> parameters;
	
	public MethodStats(String methodSignature) {
		super();
		lines = new ArrayList<>();
		this.methodSignature = methodSignature;
	}
	
	public String getmethodSignature() {
		return methodSignature;
	}
	public void setmethodSignature(String methodSignature) {
		this.methodSignature = methodSignature;
	}
	public int getCyclocomplexity() {
		return cycloComplexity;
	}
	
	public int getNumberOfLines(){
		return this.getLines().size() - 1;
	}
	
	public void setCyclocomplexity(int cyclocomplexity) {
		this.cycloComplexity = cyclocomplexity;
	}

	public ArrayList<String> getLines() {
		return lines;
	}

	public void setLines(ArrayList<String> lines) {
		this.lines = lines;
	}

	@Override
	public String toString() {
		return methodSignature +";"+ cycloComplexity  +";"+ getNumberOfLines() +";"+ getNumberOfParameters();
	}
	
	
	public void calculateCyclomaticComplexity() {
		this.setCyclocomplexity(Utils.getCyclomaticComplexity(getLines()));
	}


	public int getNumberOfParameters() {
		return this.getParameters().size();
	}

	public ArrayList<String> getParameters() {
		return parameters;
	}

	public void setParameters(ArrayList<String> parameters) {
		this.parameters = parameters;
	}
	
	
	

}
