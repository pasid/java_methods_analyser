*********JAVA METHODS ANALYSER  *********


# GOAL
Provide simple statistics related to static code analysis of a java project recursively.

# HOW IT WORKS?
-> In the main.Main class you have to indicate the projects directory that will be analyzed and just run it. 


# WHAT THE REPORT SHOWS?

For the whole project
1 - Number of Lines of Code
2 - Cyclomatic Complexity

For each class 
1 - Number of Lines of Code
2 - Cyclomatic Complexity
3 - Class Name

For each method: 

1 - Number of Lines of Code
2 - Cyclomatic Complexity	
3 - Number of Parameters
4 - Method Signature